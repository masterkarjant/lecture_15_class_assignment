import { useState } from "react";

function App() {
    const [number1, setNumber1] = useState(0)
    const [number2, setNumber2] = useState(0)
    const [number3, setNumber3] = useState(0)

    const NumberButton = ({ number, setter }) => {
        const setNumber = () => setter(number + 1)
        return <button onClick={setNumber}>{number}</button>
    }
    return (
        <div>
            <NumberButton number={number1} setter={setNumber1} />
            <NumberButton number={number2} setter={setNumber2} />
            <NumberButton number={number3} setter={setNumber3} />
            <div>{number1 + number2 + number3}</div>
        </div>
    )

    // OPTIONAL
    // return (
    //     <div>
    //         <button onClick={() => setNumber1(number1 + 1)}>{number1}</button>
    //         <button onClick={() => setNumber2(number2 + 1)}>{number2}</button>
    //         <button onClick={() => setNumber3(number3 + 1)}>{number3}</button>
    //         <div>{number1 + number2 + number3}</div>
    //     </div>
    // )
}