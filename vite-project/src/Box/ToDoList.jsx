import { useState } from 'react'

// id voi olla täällä niin toimii

function App() {

  const [toDoItemList, setToDoItemList] = useState([])
  const [toDoIdMax, setToDoIdMax] = useState(0)

  const [inputValue, setInputValue] = useState("");
  //|
  const onInput = (event) => {
    setInputValue(event.target.value);
  }

  const addToDoItem = () => {
    setToDoIdMax(toDoIdMax + 1)
    setToDoItemList(toDoItemList.concat({ id: toDoIdMax, task: inputValue }))
    setInputValue("");
  }

  const removeTask = (id) => {
    setToDoItemList(toDoItemList.filter(task => task.id != id))
  }

  const ListOfTasks = () => {
    const listDom = toDoItemList.map((item, i) => {
      return <p key={i} onClick={() => removeTask(item.id)}><i>{item.task}</i></p>;
    })
    return listDom
  }

  return (
    <div>
      <p>Add Todo Item</p>
      <input value={inputValue} onChange={onInput}></input>
      <button onClick={addToDoItem}>Submit</button>
      <ListOfTasks />
    </div>
  )
}
export default App